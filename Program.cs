﻿using System;

namespace ex25
{
    class FieldClass
    {
        // Task A //
        private int Field01;
        private int Field02;

        public FieldClass (int num1, int num2)
        {
            Field01 = num1;
            Field02 = num2;

            this.PrintNumbers();
        }

        private void PrintNumbers()
        {
            Console.WriteLine($"Number01 : {Field01}, Number02 : {Field02}");
        }  
    }
        // Task B //
    class Checkout
    {
        
            private int Quantity;
            private double Price;
        
        public Checkout(int Quantity, double Price)
        {
            this.Quantity = Quantity;
            this.Price = Price;
        }
        public double PrintTotal()
        {
            return Convert.ToDouble(this.Quantity) * this.Price;
        }     
    }
    class Program
    {
        static void Main(string[] args)
        {
            // a //
            var a = new FieldClass(5, 6);
            
            // b //
            var a1 = new Checkout(3, 4.50);
            var a2 = new Checkout(4, 5.50);
            var a3 = new Checkout(5, 6.50);
            var a4 = new Checkout(6, 7.50);
            var a5 = new Checkout(7, 8.50);

            Console.WriteLine(a1.PrintTotal()+a2.PrintTotal()+a3.PrintTotal()+a4.PrintTotal()+a5.PrintTotal());
        }
    }
}
